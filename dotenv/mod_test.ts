import { config } from "./mod.ts";
import {
  assertThrows,
    assertEquals
    } from "https://deno.land/std/testing/asserts.ts";

Deno.test(function configure() {
  let conf = config();
    assertEquals(conf.GREETING, "hello world", "fetches .env by default");
    });
