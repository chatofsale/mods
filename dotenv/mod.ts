export interface DotenvConfig {
  [key: string]: string;
  }

  export interface ConfigOptions {
    path?: string;
      export?: boolean;
        safe?: boolean;
	  example?: string;
	    allowEmptyValues?: boolean;
	    }

	    export function parse(rawDotenv: string): DotenvConfig {
	      return rawDotenv.split("\n").reduce((acc:Record<string,any>, line) => {
		          if (!isVariableStart(line)) return acc;
			      let [key, ...vals] = line.split("=");
			          let value = vals.join("=");
				      if (/^"/.test(value)) {
				            value = expandNewlines(value);
					        }
						    acc[key] = cleanQuotes(value);
						        return acc;
							  }, {});
							  }

							  export function config(options: ConfigOptions = {}): DotenvConfig {
							    const o: ConfigOptions = Object.assign(
							        {
								      path: `${Deno.cwd()}/.env`,
								            export: false,
									          safe: false,
										        example: `${Deno.cwd()}/.env.example`,
											      allowEmptyValues: false
											          },
												      options
												        );

													  const conf = parseFile(o.path);

													    if (o.safe) {
													        const confExample = parseFile(o.example);
														    // assertSafe(conf, confExample, o.allowEmptyValues);
														      }

														        if (o.export) {
															    const currentEnv = Deno.env();
															        for (let key in conf) {
																      currentEnv[key] = conf[key];
																          }
																	    }

																	      return conf;
																	      }

																	      function parseFile(filepath: any) {
																	        return parse(new TextDecoder("utf-8").decode(Deno.readFileSync(filepath)));
																		}

																		function isVariableStart(str: string): boolean {
																		  return /^[a-zA-Z_]*=/.test(str);
																		  }

																		  function cleanQuotes(value: string = ""): string {
																		    return value.replace(/^['"]([\s\S]*)['"]$/g, "$1");
																		    }

																		    function expandNewlines(str: string): string {
																		      return str.replace("\\n", "\n");
																		      }

																		      // function assertSafe(conf, confExample, allowEmptyValues) {
																		      //   const currentEnv = Deno.env();

																		      //   // Not all the variables have to be defined in .env, they can be supplied externally
																		      //   const confWithEnv = Object.assign({}, currentEnv, conf);


																		      // }

																		      export class MissingEnvVarsError extends Error {
																		        constructor(message?: string) {
																			    super(message);
																			        this.name = "MissingEnvVarsError";
																				    Object.setPrototypeOf(this, new.target.prototype);
																				      }
																				      }
