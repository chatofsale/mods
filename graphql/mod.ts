// export async function gql(url: string, headers?: any, query?: string, variables?: string) {
//     const res = await fetch(url, {
//         method: 'POST',
//         headers: {
//             'content-type': 'application/json',
//             ...headers
//         },
//         body: JSON.stringify({
//             query: query,
//             variables: variables,
//         })
//     });
//     let body = await res.text();
//     try {
//         body = JSON.parse(body);
//         return body
//     } catch (e) {
//         throw new Error(body);
//     }
//     return null
// }

/**
 * Http error for throw
 */
class HttpError extends Error {
  code = 0;
  message = "";

  constructor(code: number, message: string) {
    super(message);
    this.code = code;
   
   this.message = message;
  }
}

/**
 * req config
 */
export interface IConfigProps {
  GRAPHQL_URL: string;
  GRAPHQL_TOKEN?: string;
}

/**
 * req connect
 */
export interface IQueryProps {
  query: string;
  variables?: string;
}

const opts = {
    url : "",
    headers : {}
}

export const connect = function (req:IConfigProps) { 
    opts.headers = req.GRAPHQL_TOKEN ? {
      "Authorization": `Bearer ${req.GRAPHQL_TOKEN}`
    } : {}
    opts.url = req.GRAPHQL_URL
} 

export const query = async function (req:IQueryProps): Promise<any> {
    const res = await fetch(opts?.url, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            ...opts?.headers
        },
        body: JSON.stringify({
            query: req.query,
            variables: req.variables,
        })
    });
    let body = await res.text();
    try {
        body = JSON.parse(body);
        return body
    } catch (e) {
        throw new Error(body);
    }
}