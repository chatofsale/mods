import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test(function t1() {
  assertEquals("hello", "hello");
});

Deno.test(function t2() {
  assertEquals("world", "world");
});

//await Deno.runTests();
