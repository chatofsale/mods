type Reader = Deno.Reader
const { run, toAsyncIterator } = Deno
import { chkdupe } from "./chkdupe/mod.ts";
import * as pext from "./pext/mod.ts";
export {assert,assertEquals,assertStrContains} from "https://deno.land/std/testing/asserts.ts";

//// parallel, good
//for await (const req of s) {
  //~async function() {
    //const content = await getFileContent()
    //req.respond({ body: content });
  //}()
//}
async function main2() {
    const myUntrustedCode = `console.log("ccc");`;

    const proc = run({
      args: [
        "deno",
        "run",
        "--allow-read=false",
        "--alow-write=false",
        "--allow-net=false",
        "--allow-env=false",
        "--allow-import=false",
        "--allow-modify-builtin=false",
        "--cpu-max=10%",
        "--ram-max=512", 
        "--execution-time-max=10s",
        //"--import=./my-trusted-base-code.ts",
        myUntrustedCode,
      ],
      stdout: "piped",
      stderr: "piped"
    });
    for await (const message of toAsyncIterator(proc.stdout!)) {
        let stdout = new TextDecoder("utf-8").decode(message)
        console.log(stdout.replace("\n", ""))
    }
}

async function main() {
    let json = {"aaa":"aaa1", "bbb": "bbb2"}
    let resp_cmd = await pext.cmd(["echo", JSON.stringify(json)])
    //Deno.exit()
    if (resp_cmd) {
        let resp_json = JSON.parse(resp_cmd[0])
        resp_json.aaa ? console.log(resp_json.aaa) : null
    }
    
    //const encoder = new TextEncoder();
    //const data = encoder.encode(Date() + " : Hello world\n");
    //const tempDirName1 = await Deno.makeTempDir({ prefix: 'my_temp' });
    //console.log(`ls ${tempDirName1}`)
    //await Deno.writeFile(tempDirName1 + "/hello.txt", data);
    //await Deno.remove(tempDirName1 + "/hello.txt", {recursive: true}).catch(err=>{})
    //let ss = await Deno.truncate("/tmp/my_temp96f2d92c/hello.txt", 10);
    
    let ss = [1,2,1,2,3,4,5]
    
    let dd = chkdupe(ss)
    console.log(dd)
    
    for (let i = 0; i < Deno.args.length; i++) {
      let filename = Deno.args[i];
      let file = await Deno.open(filename);
      await Deno.copy(Deno.stdout, file);
      file.close();
    }
    
 }

main()