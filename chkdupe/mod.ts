export function chkdupe(client: Array<any>, hasher?: any) {
  hasher = hasher || JSON.stringify;

  const clone = [];
  const lookup:Record<string, any> = {};

  for (let i = 0; i < client.length; i++) {
    let elem = client[i];
    let hashed = hasher(elem);

    if (!lookup[hashed]) {
      clone.push(elem);
      lookup[hashed] = true;
    }
  }

  return clone;
}
