type Reader = Deno.Reader;
const { run, toAsyncIterator } = Deno;

export const cmd = async (args: any) => {
  let ret = [];

  const proc = run({
    args,
    stdout: "piped",
    stderr: "piped"
  });
  const { code } = await proc.status();
  if (code === 0) {
    for await (const message of toAsyncIterator(proc.stdout!)) {
      let stdout = new TextDecoder("utf-8").decode(message);
      ret.push(stdout.replace("\n", ""));
    }
  } else {
    const rawError = await proc.stderrOutput();
    const errorString = new TextDecoder().decode(rawError);
    console.log(errorString);
  }
  return ret;
};
